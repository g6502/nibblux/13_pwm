library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity PWM is
	port(
		clk	: in	std_logic;
		ciclo_util	: in std_logic_vector(7 downto 0);
		pwm_o	: out	std_logic
	);

end PWM;

architecture behavioral of PWM is
	signal frec	: std_logic;
	signal cont	: std_logic_vector(7 downto 0) := (others => '0');
	--signal cont	: std_logic_vector(ciclo_util'length) := (others => '0');
begin
	u0	: entity work.div_freq
		generic map(fin_cont => 120)
		port map(clk => clk,
					dclk => frec);
	process(frec)
	begin
		if rising_edge(frec) then
			if cont = "01100100" then
				cont <= (others => '0');
			else
				cont <= cont + 1;
			end if;
		end if;
	end process;
	pwm_o <= '1' when cont < ciclo_util else '0';
end behavioral;